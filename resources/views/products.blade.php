@extends('layouts.dashboard')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        <a href="create-product">
                        <h3>ایجاد محصول</h3>
                        </a>
                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>نام محصول</th>
                                    <th>دسته بندی</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="table-img">
                                            <?php
                                            $product_image = \App\Models\ProductImage::where('product_id', $product->id)->first();
                                            ?>
                                            @if(!is_null($product_image))
                                            <img style="width: 100px;height: 100px" src="{{'/product_images/'.$product_image->image}}" alt="">
                                            @endif
                                        </td>
                                        <td>{{$product->title}}</td>
                                        <td>{{$product->category->title}}</td>
                                        <td>

                                            <a class="btn tblActnBtn" href="/edit-product/{{$product->id}}">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                            <a class="btn tblActnBtn" href="/delete-product/{{$product->id}}">
                                                <i class="material-icons">mode-delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
