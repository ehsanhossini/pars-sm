<style>
    #main_nav{
        width: 100%;
        padding: 30px;
        display: flex;
        justify-content: center;
    }

    .navbar a {
        float: left;
        font-size: 16px;
        color: white;
        text-align: center;
        /*padding: 14px 16px;*/
        text-decoration: none;
    }
    #nav {
        list-style:none inside;
        margin:0;
        padding:0;
        text-align:center;
        /*transition: transform 250ms;*/
    }
    #nav li {
        display:block;
        position:relative;
        float:left;
        /*background: #24af15; !* menu background color *!*/
        color: #ffffff;
        z-index: 11;
        padding-bottom: 13px;
        padding-top: 13px;
    }
    #nav li .submenu{
        background: #4232c2;
    }
    #nav li .submenu li a{
        color: white;

    }
    #nav li .submenu li:hover{
        transition: 0.2s;
        color: #ffffff;
        background: #4232c2;
        /*border-radius: 10px ;*/
    }
    #nav li a {
        display:block;
        padding:0;
        text-decoration:none;
        width:200px; /* this is the width of the menu items */
        line-height:50px; /* this is the height of the menu items */
        color:#212529; /* list item font color */
    }


    /*#nav li:hover {*/
    /*    transition: 0.2s;*/
    /*    background:#4232c2;*/
    /*    !*border-top-left-radius:20px;*!*/
    /*    !*border-top-right-radius:20px;*!*/
    /*} */
    #category:hover {
        /*transition: 0.2s;*/
        color: white;
        background: #4232c2;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
    }
    #nav :last-child {
        border-bottom-left-radius:20px;
        border-bottom-right-radius:20px;
    }
    #nav li a:hover{
        transition: 0.2s;
        color: #ffffff;
    }
    #nav ul {
        position:absolute;
        padding:0;
        left:0;
        display:none; /* hides sublists */
    }
    #nav li:hover ul ul {display:none;} /* hides sub-sublists */
    #nav li:hover ul {display:block;} /* shows sublist on hover */
    #nav li li:hover ul {
        transition: 0.2s;
        display:block; /* shows sub-sublist on hover */
        margin-left:200px; /* this should be the same width as the parent list item */
        margin-top:-35px; /* aligns top of sub menu with top of list item */
    }
    #logo{
        position: relative;
        top: 15px;
        color:#212529;
    }
</style>
@extends('layouts.master')
<nav id="main_nav">
    <ul id="nav">
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/about-us">درباره ما</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/customer-guide"> راهنمای مشتریان</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/contact-us"> تماس با ما</a></li>
        <li class="nav-item" id="category"><a style="border-bottom-left-radius: 0px !important;border-bottom-right-radius: 0px !important;" class=" nav-link btn-outline-primary rounded-pill px-3" href="#">محصولات</a>
            <ul class="submenu">
                <?php $categories = \App\Models\ProductCategory::all(); ?>
                @foreach($categories as $category)
                    <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/products/{{$category->id}}"> {{$category->title}} </a></li>
                @endforeach

            </ul>
        </li>
        <a id="logo" class="navbar-brand h1" href="/">
            پارس صنعت ماهان
            <img src="template/img/logo.jpg" style="width: 105px">
        </a>
    </ul>
</nav>

<!-- Start Banner Hero -->
<section class="bg-light">
    <div class="container py-4">
        <div class="row align-items-center justify-content-between">
            <div class="contact-header col-lg-4">
                <h1 class="h2 pb-3 text-primary">تماس با ما</h1>
                <h3 class="h4 regular-400">جهت تماس با ما از فرم زیر استفاده نمایید</h3>
            </div>
            <div class="contact-img col-lg-5 align-items-end col-md-4">
                <img src="template/img/banner-img-01.svg">
            </div>
        </div>
    </div>
</section>
<!-- End Banner Hero -->


<!-- Start Contact -->
<section class="container py-5">

    <h1 class="col-12 col-xl-12 h2  text-primary pt-3" style="text-align: right">در اسرع وقت پاسخگو شما خواهیم بود</h1>
    <h2 class="col-12 col-xl-8 h4 text-left regular-400"></h2>
    <p class="col-12 col-xl-8 text-left text-muted pb-5 light-300">

    </p>

    <div class="row pb-4">

        <!-- Start Contact Form -->
        <div class="col-lg-8 ">
            <form class="contact-form row" method="POST" action="{{ route('contact-us')}}" role="form">

                    @csrf

                <div class="col-lg-4 mb-4">
                    <div class="form-floating">
                        <input type="text" name="name" class="form-control form-control-lg light-300" id="floatingname" name="inputname" placeholder="Name">
                        <label for="floatingname light-300" style="left: 80%">نام</label>
                    </div>
                </div><!-- End Input Name -->

                <div class="col-lg-4 mb-4">
                    <div class="form-floating">
                        <input type="text" name="email" class="form-control form-control-lg light-300" id="floatingemail" name="inputemail" placeholder="Email">
                        <label for="floatingemail light-300" style="left: 60%">آدرس ایمیل</label>
                    </div>
                </div><!-- End Input Email -->

                <div class="col-lg-4 mb-4">
                    <div class="form-floating">
                        <input type="text" name="phone" class="form-control form-control-lg light-300" id="floatingphone" name="inputphone" placeholder="Phone">
                        <label for="floatingphone light-300" style="left: 60%">شماره تلفن</label>
                    </div>
                </div><!-- End Input Phone -->


                <div class="col-12">
                    <div class="form-floating mb-4">
                        <input type="text" name="subject" class="form-control form-control-lg light-300" id="floatingsubject" name="inputsubject" placeholder="Subject">
                        <label for="floatingsubject light-300" style="left: 90%">موضوع</label>
                    </div>
                </div><!-- End Input Subject -->

                <div class="col-12">
                    <div class="form-floating mb-3 text-right">
                        <textarea class="form-control light-300" required name="message" rows="8" placeholder="Message" id="floatingtextarea"></textarea>
                        <label  for="floatingtextarea light-300" style="left: 90%">متن پیام</label>
                    </div>
                </div><!-- End Textarea Message -->

                <div class="col-md-12 col-12 m-auto">
                    <button type="submit" class="btn btn-secondary rounded-pill px-md-5 px-4 py-2 radius-0 text-light light-300">ارسال نظر</button>
                </div>

            </form>
        </div>
        <!-- End Contact Form -->


    </div>
</section>
<!-- End Contact -->

