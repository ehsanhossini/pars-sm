<style>
    #main_nav{
        width: 100%;
        padding: 30px;
        display: flex;
        justify-content: center;
    }

    .navbar a {
        float: left;
        font-size: 16px;
        color: white;
        text-align: center;
        /*padding: 14px 16px;*/
        text-decoration: none;
    }
    #nav {
        list-style:none inside;
        margin:0;
        padding:0;
        text-align:center;
        /*transition: transform 250ms;*/
    }
    #nav li {
        display:block;
        position:relative;
        float:left;
        /*background: #24af15; !* menu background color *!*/
        color: #ffffff;
        z-index: 11;
        padding-bottom: 13px;
        padding-top: 13px;
    }
    #nav li .submenu{
        background: #4232c2;
    }
    #nav li .submenu li a{
        color: white;

    }
    #nav li .submenu li:hover{
        transition: 0.2s;
        color: #ffffff;
        background: #4232c2;
        /*border-radius: 10px ;*/
    }
    #nav li a {
        display:block;
        padding:0;
        text-decoration:none;
        width:200px; /* this is the width of the menu items */
        line-height:50px; /* this is the height of the menu items */
        color:#212529; /* list item font color */
    }


    /*#nav li:hover {*/
    /*    transition: 0.2s;*/
    /*    background:#4232c2;*/
    /*    !*border-top-left-radius:20px;*!*/
    /*    !*border-top-right-radius:20px;*!*/
    /*} */
    #category:hover {
        /*transition: 0.2s;*/
        color: white;
        background: #4232c2;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
    }
    #nav :last-child {
        border-bottom-left-radius:20px;
        border-bottom-right-radius:20px;
    }
    #nav li a:hover{
        transition: 0.2s;
        color: #ffffff;
    }
    #nav ul {
        position:absolute;
        padding:0;
        left:0;
        display:none; /* hides sublists */
    }
    #nav li:hover ul ul {display:none;} /* hides sub-sublists */
    #nav li:hover ul {display:block;} /* shows sublist on hover */
    #nav li li:hover ul {
        transition: 0.2s;
        display:block; /* shows sub-sublist on hover */
        margin-left:200px; /* this should be the same width as the parent list item */
        margin-top:-35px; /* aligns top of sub menu with top of list item */
    }
    #logo{
        position: relative;
        top: 15px;
        color:#212529;
    }
</style>
@extends('layouts.master')
<nav id="main_nav">
    <ul id="nav">
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/about-us">درباره ما</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/customer-guide"> راهنمای مشتریان</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/contact-us"> تماس با ما</a></li>
        <li class="nav-item" id="category"><a style="border-bottom-left-radius: 0px !important;border-bottom-right-radius: 0px !important;" class=" nav-link btn-outline-primary rounded-pill px-3" href="#">محصولات</a>
            <ul class="submenu">
                @foreach($categories as $category)
                    <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/products/{{$category->id}}"> {{$category->title}} </a></li>
                @endforeach

            </ul>
        </li>
        <a id="logo" class="navbar-brand h1" href="/">
            پارس صنعت ماهان
            <img src="template/img/logo.jpg" style="width: 105px">
        </a>
    </ul>
</nav>
{{--<nav id="main_nav" class="navbar navbar-expand-lg navbar-light bg-white shadow">--}}
{{--    <div class="container d-flex justify-content-between align-items-center">--}}

{{--        <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse"--}}
{{--                data-bs-target="#navbar-toggler-success" aria-controls="navbarSupportedContent" aria-expanded="false"--}}
{{--                aria-label="Toggle navigation">--}}
{{--            <span class="navbar-toggler-icon"></span>--}}
{{--        </button>--}}
{{--        <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between"--}}
{{--             id="navbar-toggler-success">--}}
{{--            <div class="flex-fill mx-xl-1 mb-1">--}}
{{--                <ul class="nav navbar-nav d-flex justify-content-between mx-xl-5 text-center text-dark">--}}
{{--                    <li class="nav-item">--}}
{{--                                                <a class="nav-link btn-outline-primary rounded-pill px-4" href="index.html">Home</a>--}}
{{--                        <div class="subnav">--}}
{{--                            <button class="subnavbtn nav-link btn-outline-primary rounded-pill px-4">Services <i class="fa fa-caret-down"></i></button>--}}
{{--                            <div class="subnav-content">--}}
{{--                                <a href="#bring">محافظ پنل</a>--}}
{{--                                <a href="#deliver">منبع تغذیه</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    <a href="#">SUB SUB LIST »</a>--}}
{{--                        <ul>--}}
{{--                            <li><a href="#">Sub Sub Item 1</a>--}}
{{--                            <li><a href="#">Sub Sub Item 2</a>--}}
{{--                        </ul>--}}


{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link btn-outline-primary rounded-pill px-4" href="about-us">درباره ما</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link btn-outline-primary rounded-pill px-4" href="customer-guide">راهنمای--}}
{{--                            مشتریان</a>--}}
{{--                    </li>--}}
{{--                                        <li class="nav-item subnav">--}}

{{--                                                <button class="subnavbtn nav-link btn-outline-primary rounded-pill px-4">راهنمای مشتریان <i class="fa fa-caret-down"></i></button>--}}
{{--                                                <div class="subnav-content">--}}
{{--                                                    <ul style="list-style-type: none;">--}}
{{--                                                        <li>--}}
{{--                                                            <a class="nav-link btn-outline-primary rounded-pill px-4" style="color: white !important;margin: 1px" href="about-us">درباره ما</a>--}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <a class="nav-link btn-outline-primary rounded-pill px-4" style="color: white !important;margin: 1px " href="google.com">درباره ما</a>--}}
{{--                                                        </li>--}}
{{--                                                    </ul>--}}
{{--                                                </div>--}}
{{--                                        </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link btn-outline-primary rounded-pill px-4" href="contact-us">تماس با ما</a>--}}
{{--                    </li>--}}

{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link btn-outline-primary rounded-pill px-4" href="products">محصولات</a>--}}
{{--                    </li>--}}

{{--                </ul>--}}
{{--            </div>--}}
{{--            <a class="navbar-brand h1" href="/">--}}
{{--                پارس صنعت ماهان--}}
{{--                <img src="template/img/logo.jpg" style="width: 105px">--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</nav>--}}


<div class="banner-wrapper bg-light">
    <div id="index_banner" class="banner-vertical-center-index container-fluid pt-5">

        <!-- Start slider -->
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <ol class="carousel-indicators">
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>
                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                @foreach($sliders as $slider)
                    <div class="carousel-item active">
                        <div class="py-5 row d-flex align-items-center">
                            <div class="banner-content col-lg-8 col-8 offset-2 m-lg-auto text-left py-5 pb-5">
                                <h1 class="banner-heading h1 text-secondary display-3 mb-0 pb-5 mx-0 px-0 light-300 typo-space-line">
                                    <strong>{{$slider->title}}</strong>
                                </h1>
                                <p class="banner-body text-muted py-3 mx-0 px-0">
                                    {{$slider->description}}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev text-decoration-none" href="#carouselExampleIndicators" role="button"
               data-bs-slide="prev">
                <i class='bx bx-chevron-left'></i>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next text-decoration-none" href="#carouselExampleIndicators" role="button"
               data-bs-slide="next">
                <i class='bx bx-chevron-right'></i>
                <span class="visually-hidden">Next</span>
            </a>
        </div>
        <!-- End slider -->

    </div>
</div>

<!-- Start Service -->
<section class="service-wrapper py-3">
    <div class="container-fluid pb-3">
        <div class="row">
            <h2 class="h2 text-center col-12 py-5 semi-bold-600">اخبار</h2>
            <div class="service-header col-2 col-lg-3 text-end light-300"></div>
            <div class="service-header col-2 col-lg-3 text-end light-300"></div>
            <div class="service-header col-2 col-lg-3 text-end light-300" style="margin-left: 10%">
                <i class='bx bx-news h3 mt-1'></i>
            </div>
            {{--            <div class="service-heading col-10 col-lg-9 text-start float-end light-300">--}}
            {{--                <h2 class="h3 pb-4 ">Make Success for future</h2>--}}
            {{--            </div>--}}
        </div>
        <div class="row">
            <div class="service-header col-2 col-lg-12 text-end light-300"></div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                @foreach($news as $new)
                    <p class=" text-start  text-muted" style="text-align: right !important; direction: rtl;">
                        {{$new->title}} :
                    </p>
                    <p class=" typo-space-line  text-start  text-muted "
                       style="text-align: right !important;direction: rtl;">
                        {{$new->description}}
                    </p>
                @endforeach
            </div>
        </div>

    </div>

    <div class="service-tag py-5 bg-secondary">
        <div class="col-md-12">
            <ul class="nav d-flex justify-content-center">
                @foreach($categories as $category)
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary active shadow rounded-pill text-light px-4 light-300"
                           href="#" data-filter=".{{$category->id}}">{{$category->title}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

</section>

<section class="container overflow-hidden py-5">
    <div class="row gx-5 gx-sm-3 gx-lg-5 gy-lg-5 gy-3 pb-3 projects">

        @foreach($products as $product)
            <div class="col-xl-3 col-md-4 col-sm-6 project  {{$product->category->id}} ">
                <a href="#" class="service-work card border-0 text-white shadow-sm overflow-hidden mx-5 m-sm-0">
                    <?php
                    $product_image = \App\Models\ProductImage::where('product_id', $product->id)->first();
                    ?>
                    @if(!is_null($product_image))
                        <img class="service card-img" src="{{'/product_images/'.$product_image->image}}"
                             alt="Card image">
                    @endif

                    <div class="service-work-vertical card-img-overlay d-flex align-items-end">
                        <div class="service-work-content text-left text-light">
                            <span
                                class="btn btn-outline-light rounded-pill mb-lg-3 px-lg-4 light-300">{{$product->title}}</span>

                            <p class="card-text">{{$product->description}}</p>
                        </div>
                    </div>
                </a>
            </div>

        @endforeach
    </div>
</section>
<!-- End Service -->


<!-- Bootstrap -->
<script src="assets/js/bootstrap.bundle.min.js"></script>
<!-- Lightbox -->
<script src="assets/js/fslightbox.js"></script>
<script>
    fsLightboxInstances['gallery'].props.loadOnlyCurrentSource = true;
</script>
<!-- Load jQuery require for isotope -->
<script src="assets/js/jquery.min.js"></script>
<!-- Isotope -->
<script src="assets/js/isotope.pkgd.js"></script>
<!-- Page Script -->
<script>
    $(window).load(function () {
        // init Isotope
        var $projects = $('.projects').isotope({
            itemSelector: '.project',
            layoutMode: 'fitRows'
        });
        $(".filter-btn").click(function () {
            var data_filter = $(this).attr("data-filter");
            $projects.isotope({
                filter: data_filter
            });
            $(".filter-btn").removeClass("active");
            $(".filter-btn").removeClass("shadow");
            $(this).addClass("active");
            $(this).addClass("shadow");
            return false;
        });
    });
</script>
<!-- Templatemo -->
<script src="assets/js/templatemo.js"></script>
<!-- Custom -->
<script src="assets/js/custom.js"></script>
