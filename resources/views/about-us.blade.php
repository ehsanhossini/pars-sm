<style>
    #main_nav{
        width: 100%;
        padding: 30px;
        display: flex;
        justify-content: center;
    }

    .navbar a {
        float: left;
        font-size: 16px;
        color: white;
        text-align: center;
        /*padding: 14px 16px;*/
        text-decoration: none;
    }
    #nav {
        list-style:none inside;
        margin:0;
        padding:0;
        text-align:center;
        /*transition: transform 250ms;*/
    }
    #nav li {
        display:block;
        position:relative;
        float:left;
        /*background: #24af15; !* menu background color *!*/
        color: #ffffff;
        z-index: 11;
        padding-bottom: 13px;
        padding-top: 13px;
    }
    #nav li .submenu{
        background: #4232c2;
    }
    #nav li .submenu li a{
        color: white;

    }
    #nav li .submenu li:hover{
        transition: 0.2s;
        color: #ffffff;
        background: #4232c2;
        /*border-radius: 10px ;*/
    }
    #nav li a {
        display:block;
        padding:0;
        text-decoration:none;
        width:200px; /* this is the width of the menu items */
        line-height:50px; /* this is the height of the menu items */
        color:#212529; /* list item font color */
    }


    /*#nav li:hover {*/
    /*    transition: 0.2s;*/
    /*    background:#4232c2;*/
    /*    !*border-top-left-radius:20px;*!*/
    /*    !*border-top-right-radius:20px;*!*/
    /*} */
    #category:hover {
        /*transition: 0.2s;*/
        color: white;
        background: #4232c2;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
    }
    #nav :last-child {
        border-bottom-left-radius:20px;
        border-bottom-right-radius:20px;
    }
    #nav li a:hover{
        transition: 0.2s;
        color: #ffffff;
    }
    #nav ul {
        position:absolute;
        padding:0;
        left:0;
        display:none; /* hides sublists */
    }
    #nav li:hover ul ul {display:none;} /* hides sub-sublists */
    #nav li:hover ul {display:block;} /* shows sublist on hover */
    #nav li li:hover ul {
        transition: 0.2s;
        display:block; /* shows sub-sublist on hover */
        margin-left:200px; /* this should be the same width as the parent list item */
        margin-top:-35px; /* aligns top of sub menu with top of list item */
    }
    #logo{
        position: relative;
        top: 15px;
        color:#212529;
    }
</style>
@extends('layouts.master')
<nav id="main_nav">
    <ul id="nav">
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/about-us">درباره ما</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/customer-guide"> راهنمای مشتریان</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/contact-us"> تماس با ما</a></li>
        <li class="nav-item" id="category"><a style="border-bottom-left-radius: 0px !important;border-bottom-right-radius: 0px !important;" class=" nav-link btn-outline-primary rounded-pill px-3" href="#">محصولات</a>
            <ul class="submenu">
                <?php $categories = \App\Models\ProductCategory::all(); ?>
                @foreach($categories as $category)
                    <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/products/{{$category->id}}"> {{$category->title}} </a></li>
                @endforeach

            </ul>
        </li>
        <a id="logo" class="navbar-brand h1" href="/">
            پارس صنعت ماهان
            <img src="template/img/logo.jpg" style="width: 105px">
        </a>
    </ul>
</nav>

<!-- Start Banner Hero -->
<section class="bg-light w-100">
    <div class="container">
        <div class="row d-flex align-items-center py-5">
            <div class="col-lg-6 text-start">
                <h1 class="h2 py-5 text-primary typo-space-line" style="width: 200px;float: right">درباره ما</h1>
                <div class="clearfix"></div>
                <p class="light-300" style="text-align: right">
                    شرکت پارس صنعت ماهان فعالیت خود را در زمینه تولید محافظ پنل دربازکن های صوتی و تصویری از سال 1386 در استان تهران آغاز نمود. در ابتدا این شرکت تنها به تولید محافظ های فلزی پرداخت ولی پس از گذشت یکسال و با توجه به نیازسنجی بازار و نیز ارتقا سطح کیفی محصولات خود، اقدام به تولید و عرضه محافظ هایی از جنس ABS و پلی کربنات نمود.

                    با تداوم استقبال مشتریان نسبت به محصولات جدید ، لزوم بهره گیری از مدل های بروز این دسته از محافظ ها با قابلیت های بیشتر، ضروری به نظر می آمد. اکنون، با تولید انواع محافظ در رنگ های مختلف و با قابلیت نصب بر روی انواع پنل های موجود در بازار ، عملاً سبد کالایی در این زمینه تکمیل گردیده است.

                </p>
            </div>
            <div class="col-lg-6">
                <img src="template/img/banner-img-02.svg">
            </div>
        </div>
    </div>
</section>

