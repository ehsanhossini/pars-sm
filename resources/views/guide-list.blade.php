<style>
    #main_nav{
        width: 100%;
        padding: 30px;
        display: flex;
        justify-content: center;
    }

    .navbar a {
        float: left;
        font-size: 16px;
        color: white;
        text-align: center;
        /*padding: 14px 16px;*/
        text-decoration: none;
    }
    #nav {
        list-style:none inside;
        margin:0;
        padding:0;
        text-align:center;
        /*transition: transform 250ms;*/
    }
    #nav li {
        display:block;
        position:relative;
        float:left;
        /*background: #24af15; !* menu background color *!*/
        color: #ffffff;
        z-index: 11;
        padding-bottom: 13px;
        padding-top: 13px;
    }
    #nav li .submenu{
        background: #4232c2;
    }
    #nav li .submenu li a{
        color: white;

    }
    #nav li .submenu li:hover{
        transition: 0.2s;
        color: #ffffff;
        background: #4232c2;
        /*border-radius: 10px ;*/
    }
    #nav li a {
        display:block;
        padding:0;
        text-decoration:none;
        width:200px; /* this is the width of the menu items */
        line-height:50px; /* this is the height of the menu items */
        color:#212529; /* list item font color */
    }


    /*#nav li:hover {*/
    /*    transition: 0.2s;*/
    /*    background:#4232c2;*/
    /*    !*border-top-left-radius:20px;*!*/
    /*    !*border-top-right-radius:20px;*!*/
    /*} */
    #category:hover {
        /*transition: 0.2s;*/
        color: white;
        background: #4232c2;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
    }
    #nav :last-child {
        border-bottom-left-radius:20px;
        border-bottom-right-radius:20px;
    }
    #nav li a:hover{
        transition: 0.2s;
        color: #ffffff;
    }
    #nav ul {
        position:absolute;
        padding:0;
        left:0;
        display:none; /* hides sublists */
    }
    #nav li:hover ul ul {display:none;} /* hides sub-sublists */
    #nav li:hover ul {display:block;} /* shows sublist on hover */
    #nav li li:hover ul {
        transition: 0.2s;
        display:block; /* shows sub-sublist on hover */
        margin-left:200px; /* this should be the same width as the parent list item */
        margin-top:-35px; /* aligns top of sub menu with top of list item */
    }
    #logo{
        position: relative;
        top: 15px;
        color:#212529;
    }
</style>
@extends('layouts.master')
<nav id="main_nav">
    <ul id="nav">
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/about-us">درباره ما</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/customer-guide"> راهنمای مشتریان</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/contact-us"> تماس با ما</a></li>
        <li class="nav-item" id="category"><a style="border-bottom-left-radius: 0px !important;border-bottom-right-radius: 0px !important;" class=" nav-link btn-outline-primary rounded-pill px-3" href="#">محصولات</a>
            <ul class="submenu">
                <?php $categories = \App\Models\ProductCategory::all(); ?>
                @foreach($categories as $category)
                    <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/products/{{$category->id}}"> {{$category->title}} </a></li>
                @endforeach

            </ul>
        </li>
        <a id="logo" class="navbar-brand h1" href="/">
            پارس صنعت ماهان
            <img src="template/img/logo.jpg" style="width: 105px">
        </a>
    </ul>
</nav>


<!-- Start Banner Hero -->
<div id="work_banner" class="banner-wrapper bg-light w-100 py-8" style="height: 200px">
    <div class="banner-vertical-center-work container text-light d-flex justify-content-center align-items-center py-5 p-0">
        <div class="banner-content col-lg-8 col-12 m-lg-auto text-center">
            <h1 class="banner-heading h2 display-3 pb-5 semi-bold-600 typo-space-line-center"> راهنمای مشتریان</h1>
{{--            <p class="banner-body pb-2 light-300">--}}
{{--                Vector illustration <a class="text-white" href="http://freepik.com/" target="_blank">Freepik</a>.--}}
{{--                Lorem ipsum dolor sit amet, consectetur adipiscing elit,--}}
{{--                sed do eiusmod tempor incididunt ut labore et dolore magna--}}
{{--                aliqua. Quis ipsum suspendisse ultrices gravida. Risus--}}
{{--                commodo viverra maecenas accumsan lacus.--}}
{{--            </p>--}}
        </div>
    </div>
</div>
<!-- End Banner Hero -->

<!-- Start Our Work -->
<section class="container py-5">


{{--        <a href="work-single.html" class="col-sm-6 col-lg-4 text-decoration-none project marketing social business">--}}
{{--            <div class="service-work overflow-hidden card mb-5 mx-5 m-sm-0">--}}
{{--                <img class="card-img-top" src="./assets/img/our-work-01.jpg" alt="...">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title light-300 text-dark">Digital Marketing</h5>--}}
{{--                    <p class="card-text light-300 text-dark">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,--}}
{{--                        sed do eiusmod tempor incididunt ut labore et dolor.--}}
{{--                    </p>--}}
{{--                    <span class="text-decoration-none text-primary light-300">--}}
{{--                              Read more <i class='bx bxs-hand-right ms-1'></i>--}}
{{--                          </span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </a>--}}
        <div class="container text-right" style="direction: rtl;margin-bottom: 10px">
        @foreach($guides as $guide)
            <div >
            <h5 class="card-title light-300 text-dark">{{$guide->title}}</h5>
            <a class="btn btn-primary" href="customer_guides\{{$guide->file}}" download>دانلود</a>
            </div>

        @endforeach
        </div>
        <div class="clearfix"></div>


</section>
<!-- End Our Work -->
