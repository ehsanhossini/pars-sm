@extends('layouts.dashboard')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        <a href="create-news">
                            <h3>ایجاد اخبار</h3>
                        </a>
                        <div class="clearfix"></div>
                        <div class="table-responsive" style="overflow: inherit ">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>عنوان اخبار </th>
                                    <th> توصیحات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($news as $new)
                                    <tr>

                                        <td>{{$new->title}}</td>
                                        <td>{{$new->description}}</td>
                                        <td>

                                            <a class="btn tblActnBtn" href="/edit-news/{{$new->id}}">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                            <a class="btn tblActnBtn" href="/delete-news/{{$new->id}}">
                                                <i class="material-icons">mode-delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
