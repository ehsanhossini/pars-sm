<!DOCTYPE html>
<html lang="en">

<head>
    <title>پارس صنعت ماهان</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="template/img/apple-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="../template/img/logo1.png">
    <!-- Load Require CSS -->
    <link href="../template/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font CSS -->
    <link href="../template/css/boxicon.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <!-- Load Tempalte CSS -->
    <link rel="stylesheet" href="../template/css/templatemo.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../template/css/custom.css">
    <style>
        #main_nav{
            width: 100%;
            padding: 30px;
            display: flex;
            justify-content: center;
        }

        .navbar a {
            float: left;
            font-size: 16px;
            color: white;
            text-align: center;
            /*padding: 14px 16px;*/
            text-decoration: none;
        }
        #nav {
            list-style:none inside;
            margin:0;
            padding:0;
            text-align:center;
            /*transition: transform 250ms;*/
        }
        #nav li {
            display:block;
            position:relative;
            float:left;
            /*background: #24af15; !* menu background color *!*/
            color: #ffffff;
            z-index: 11;
            padding-bottom: 13px;
            padding-top: 13px;
        }
        #nav li .submenu{
            background: #4232c2;
        }
        #nav li .submenu li a{
            color: white;

        }
        #nav li .submenu li:hover{
            transition: 0.2s;
            color: #ffffff;
            background: #4232c2;
            /*border-radius: 10px ;*/
        }
        #nav li a {
            display:block;
            padding:0;
            text-decoration:none;
            width:200px; /* this is the width of the menu items */
            line-height:50px; /* this is the height of the menu items */
            color:#212529; /* list item font color */
        }


        /*#nav li:hover {*/
        /*    transition: 0.2s;*/
        /*    background:#4232c2;*/
        /*    !*border-top-left-radius:20px;*!*/
        /*    !*border-top-right-radius:20px;*!*/
        /*} */
        #category:hover {
            /*transition: 0.2s;*/
            color: white;
            background: #4232c2;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
        }
        #nav :last-child {
            border-bottom-left-radius:20px;
            border-bottom-right-radius:20px;
        }
        #nav li a:hover{
            transition: 0.2s;
            color: #ffffff;
        }
        #nav ul {
            position:absolute;
            padding:0;
            left:0;
            display:none; /* hides sublists */
        }
        #nav li:hover ul ul {display:none;} /* hides sub-sublists */
        #nav li:hover ul {display:block;} /* shows sublist on hover */
        #nav li li:hover ul {
            transition: 0.2s;
            display:block; /* shows sub-sublist on hover */
            margin-left:200px; /* this should be the same width as the parent list item */
            margin-top:-35px; /* aligns top of sub menu with top of list item */
        }
        #logo{
            position: relative;
            top: 15px;
            color:#212529;
        }
    </style>
</head>

<body>
<!-- Header -->

<!-- Close Header -->
<nav id="main_nav">
    <ul id="nav">
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/about-us">درباره ما</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/customer-guide"> راهنمای مشتریان</a></li>
        <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/contact-us"> تماس با ما</a></li>
        <li class="nav-item" id="category"><a style="border-bottom-left-radius: 0px !important;border-bottom-right-radius: 0px !important;" class=" nav-link btn-outline-primary rounded-pill px-3" href="#">محصولات</a>
            <ul class="submenu">
                @foreach($categories as $category)
                    <li class="nav-item"><a class="nav-link btn-outline-primary rounded-pill px-3" href="/products/{{$category->id}}"> {{$category->title}} </a></li>
                @endforeach

            </ul>
        </li>
        <a id="logo" class="navbar-brand h1" href="/">
            پارس صنعت ماهان
            <img src="../template/img/logo.jpg" style="width: 105px">
        </a>
    </ul>
</nav>

<section class="container py-5">


    <div class="row projects gx-lg-5">

        @foreach($products as $product)
            <a href="work-single.html" class="col-sm-6 col-lg-4 text-decoration-none project {{$product->category->id}} ">
                <div class="service-work overflow-hidden card mb-5 mx-5 m-sm-0">
                    <?php
                    $product_image = \App\Models\ProductImage::where('product_id', $product->id)->first();
                    ?>
                    @if(!is_null($product_image))
                        <img class="card-img-top" src="{{'/product_images/'.$product_image->image}}" alt="Card image">
                    @endif

                    <div class="card-body">
                        <h5 class="card-title light-300 text-dark">{{$product->title}}</h5>
                        <p class="card-text light-300 text-dark">
                            {{$product->description}}
                        </p>
                        {{--                        <span class="text-decoration-none text-primary light-300">--}}
                        {{--                              Read more <i class='bx bxs-hand-right ms-1'></i>--}}
                        {{--                          </span>--}}
                    </div>
                </div>
            </a>
        @endforeach
    </div>

</section>




<!-- Start Footer -->
<footer class="bg-secondary pt-4">
    <div class="container">
        <div class="row py-4">
            <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                <h2 class="h4 pb-lg-3 text-light light-300" > پیوندها </h2>
                <ul class="list-unstyled text-light light-300">
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://irancommax.com/" target="_blank">ایران کوماکس</a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://www.electropeyk.com/" target="_blank">الکتروپیک </a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://tabaproduct.com/" target="_blank">تابا </a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://simaran.com/" target="_blank"> سیماران</a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://suzuki-vdp.com/" target="_blank">سوزوکی</a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://hedayat-fermax.com/" target="_blank">هدایت</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                <h2 class="h4 pb-lg-3 text-light light-300" > پست الکترونیکی</h2>
                <ul class="list-unstyled text-light light-300">
                    <li class="pb-2">
                        <i class='bx-fw bx bx-mail-send bx-xs'></i><a class="text-decoration-none text-light py-1" href="mailto:info@company.com">pars.s.mahan@gmail.com</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                <h3 class="h4 pb-lg-3 text-light light-300"> لینک ها</h2>
                    <ul class="list-unstyled text-light light-300">
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light" href="/">صفحه اصلی</a>
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="about-us"> درباره ما</a>
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="products"> محصولات</a>
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="contact-us">تماس با ما</a>
                        </li>
                    </ul>
            </div>



            <div class="col-lg-3 col-12 align-left">
                <a class="navbar-brand" href="index.html">
                    <span class="text-light h5">  پارس صنعت ماهان</span>
                    <img src="../template/img/logo.jpg" style="width: 105px">
                </a>
                <p class="text-light my-lg-4 my-2">
                    شرکت پارس صنعت ماهان فعالیت خود را در زمینه تولید محافظ پنل دربازکن های صوتی و تصویری از سال 1386 در استان تهران آغاز نمود. در ابتدا این شرکت تنها به تولید محافظ های فلزی پرداخت ولی پس از گذشت یکسال و با توجه به نیازسنجی بازار و نیز ارتقا سطح کیفی محصولات خود، اقدام به تولید و عرضه محافظ هایی از جنس ABS و پلی کربنات نمود
                </p>
                <ul class="list-inline footer-icons light-300">
                    <li class="list-inline-item m-0">
                        <a class="text-light" target="_blank" href="https://www.linkedin.com/">
                            <i class='bx bxl-linkedin-square bx-md'></i>
                        </a>
                    </li>
                    <li class="list-inline-item m-0">
                        <a class="text-light" target="_blank" href="https://wa.me/989195666816">
                            <i class='bx bxl-whatsapp-square bx-md'></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="w-100 bg-primary py-3">
        <div class="container">
            <div class="row pt-2">
                <div class="col-lg-6 col-sm-12">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <p class="text-lg-end text-center text-light light-300">
                        کلیه حقوق و امتیازات این وبسایت متعلق به پارس صنعت ماهان می باشد ©
                    </p>
                </div>
            </div>
        </div>
    </div>


</footer>
<!-- End Footer -->


<!-- Bootstrap -->
<script src="../template/js/bootstrap.bundle.min.js"></script>
<!-- Load jQuery require for isotope -->
<script src="../template/js/jquery.min.js"></script>
<!-- Isotope -->
<script src="../template/js/isotope.pkgd.js"></script>
<!-- Page Script -->
<script>
    $(window).load(function() {
        // init Isotope
        var $projects = $('.projects').isotope({
            itemSelector: '.project',
            layoutMode: 'fitRows'
        });
        $(".filter-btn").click(function() {
            var data_filter = $(this).attr("data-filter");
            $projects.isotope({
                filter: data_filter
            });
            $(".filter-btn").removeClass("active");
            $(".filter-btn").removeClass("shadow");
            $(this).addClass("active");
            $(this).addClass("shadow");
            return false;
        });
    });
</script>
<!-- Templatemo -->
<script src="../template/js/templatemo.js"></script>
<!-- Custom -->
<script src="../template/js/custom.js"></script>

</body>

</html>
