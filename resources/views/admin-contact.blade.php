@extends('layouts.dashboard')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">

                        <div class="table-responsive" style="overflow: inherit ">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                <tr>
                                    <th>خوانده شده</th>
                                    <th>موضوع درخواست</th>
                                    <th>نام درخواست کننده </th>
                                    <th>  ایمیل درخواست کننده</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($messages as $message)
                                    <tr>
                                        <td>
                                                <i class="material-icons">
                                                    @if($message->read == true)
                                                        check
                                                    @elseif($message->read == false)
                                                        markunread
                                                    @endif
                                                </i>

                                            {{$message->subject}}</td>
                                        <td>{{$message->name}}</td>
                                        <td>{{$message->email}}</td>
                                        <td>

                                            <a class="btn tblActnBtn" href="/show-contact/{{$message->id}}">
                                                <i class="material-icons">receipt</i>
                                            </a>
                                            <a class="btn tblActnBtn" href="/delete-contact/{{$message->id}}">
                                                <i class="material-icons">mode-delete</i>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
