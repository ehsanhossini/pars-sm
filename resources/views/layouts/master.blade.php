<!DOCTYPE html>
<html lang="en">

<head>
    <title>پارس صنعت ماهان</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="template/img/apple-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="template/img/logo1.png">
    <!-- Load Require CSS -->
    <link href="template/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font CSS -->
    <link href="template/css/boxicon.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <!-- Load Tempalte CSS -->
    <link rel="stylesheet" href="template/css/templatemo.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="template/css/custom.css">

    <!--

        TemplateMo 561 Purple Buzz

        https://templatemo.com/tm-561-purple-buzz

        -->
</head>

<body>
<!-- Header -->

<!-- Close Header -->

@yield('content')

<!-- Start Footer -->
<footer class="bg-secondary pt-4">
    <div class="container">
        <div class="row py-4">
            <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                <h2 class="h4 pb-lg-3 text-light light-300" > پیوندها </h2>
                <ul class="list-unstyled text-light light-300">
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://irancommax.com/" target="_blank">ایران کوماکس</a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://www.electropeyk.com/" target="_blank">الکتروپیک </a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://tabaproduct.com/" target="_blank">تابا </a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://simaran.com/" target="_blank"> سیماران</a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://suzuki-vdp.com/" target="_blank">سوزوکی</a>
                    </li>
                    <li class="pb-2">
                        <i class='bx-fw bx bx-link bx-xs'></i>
                        <a class="text-decoration-none text-light py-1" href="http://hedayat-fermax.com/" target="_blank">هدایت</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                <h2 class="h4 pb-lg-3 text-light light-300" > پست الکترونیکی</h2>
                <ul class="list-unstyled text-light light-300">
                    <li class="pb-2">
                        <i class='bx-fw bx bx-mail-send bx-xs'></i><a class="text-decoration-none text-light py-1" href="mailto:info@company.com">pars.s.mahan@gmail.com</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                <h3 class="h4 pb-lg-3 text-light light-300"> لینک ها</h2>
                    <ul class="list-unstyled text-light light-300">
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light" href="/">صفحه اصلی</a>
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="about-us"> درباره ما</a>
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="products"> محصولات</a>
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="contact-us">تماس با ما</a>
                        </li>
                    </ul>
            </div>



            <div class="col-lg-3 col-12 align-left">
                <a class="navbar-brand" href="index.html">
                    <span class="text-light h5">  پارس صنعت ماهان</span>
                    <img src="template/img/logo.jpg" style="width: 105px">
                </a>
                <p class="text-light my-lg-4 my-2">
                    شرکت پارس صنعت ماهان فعالیت خود را در زمینه تولید محافظ پنل دربازکن های صوتی و تصویری از سال 1386 در استان تهران آغاز نمود. در ابتدا این شرکت تنها به تولید محافظ های فلزی پرداخت ولی پس از گذشت یکسال و با توجه به نیازسنجی بازار و نیز ارتقا سطح کیفی محصولات خود، اقدام به تولید و عرضه محافظ هایی از جنس ABS و پلی کربنات نمود
                </p>
                <ul class="list-inline footer-icons light-300">
                    <li class="list-inline-item m-0">
                        <a class="text-light" target="_blank" href="https://www.linkedin.com/">
                            <i class='bx bxl-linkedin-square bx-md'></i>
                        </a>
                    </li>
                    <li class="list-inline-item m-0">
                        <a class="text-light" target="_blank" href="https://wa.me/989195666816">
                            <i class='bx bxl-whatsapp-square bx-md'></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="w-100 bg-primary py-3">
        <div class="container">
            <div class="row pt-2">
                <div class="col-lg-6 col-sm-12">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <p class="text-lg-end text-center text-light light-300">
                        کلیه حقوق و امتیازات این وبسایت متعلق به پارس صنعت ماهان می باشد ©
                    </p>
                </div>
            </div>
        </div>
    </div>


</footer>
<!-- End Footer -->


<!-- Bootstrap -->
<script src="template/js/bootstrap.bundle.min.js"></script>
<!-- Load jQuery require for isotope -->
<script src="template/js/jquery.min.js"></script>
<!-- Isotope -->
<script src="template/js/isotope.pkgd.js"></script>
<!-- Page Script -->
<script>
    $(window).load(function() {
        // init Isotope
        var $projects = $('.projects').isotope({
            itemSelector: '.project',
            layoutMode: 'fitRows'
        });
        $(".filter-btn").click(function() {
            var data_filter = $(this).attr("data-filter");
            $projects.isotope({
                filter: data_filter
            });
            $(".filter-btn").removeClass("active");
            $(".filter-btn").removeClass("shadow");
            $(this).addClass("active");
            $(this).addClass("shadow");
            return false;
        });
    });
</script>
<!-- Templatemo -->
<script src="template/js/templatemo.js"></script>
<!-- Custom -->
<script src="template/js/custom.js"></script>

</body>

</html>
