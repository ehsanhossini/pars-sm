<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <title>پارس صنعت ماهان</title>
    <!-- Favicon-->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet">
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <link href="/assets/css/form.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet"/>
</head>
<body>
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30">
            <img class="loading-img-spin" src="/assets/images/loading.png" alt="admin">
        </div>
        <p>لطفا صبر کنید...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="#" onClick="return false;" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"
               aria-expanded="false"></a>
            <a href="#" onClick="return false;" class="bars"></a>
            <a class="navbar-brand" style="padding: 0px">
                <img style="width: 200px;height: 50px" src="/assets/images/logo.jpg" alt="" />
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <a href="#" onClick="return false;" class="sidemenu-collapse">
                        <i class="nav-hdr-btn ti-align-left"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<div>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar" >
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li>
                    <div class="sidebar-profile clearfix">
                        <div class="profile-img">
                            <img src="/assets/images/usrbig1.jpeg" alt="profile">
                        </div>
                        <div class="profile-info">
                            <h3> مدیر عزیز</h3>
                            <p>خوش آمدید !</p>
                        </div>
                    </div>
                </li>

                <li class="active">
                    <div class="sidebar-profile clearfix">
                    <a href="/dashboard">
                        <span>محصولات</span>
                    </a>
                    </div>
                </li>
                <li>
                    <div class="sidebar-profile clearfix">
                    <a href="/admin-news">
                        <span>اخبار</span>
                    </a>
                    </div>
                </li>
                <li>
                    <div class="sidebar-profile clearfix">
                        <a href="/admin-contact">
                            <span>پیام ها</span>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="sidebar-profile clearfix">
                        <a href="/admin-slider">
                            <span> اسلایدر</span>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="sidebar-profile clearfix">
                        <a href="/admin-customer-guide">
                            <span>ارتباط با مشتریان</span>
                        </a>
                    </div>
                </li>

            </ul>
        </div>
        <!-- #Menu -->
    </aside>

</div>
<body class="light rtl">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30">
            <img class="loading-img-spin" src="../../assets/images/loading.png" alt="admin">
        </div>
        <p>لطفا صبر کنید...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Top Bar -->
@yield('content')

<script src="/assets/js/app.min.js"></script>
<script src="/assets/js/table.min.js"></script>
<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>
<script src="/assets/js/pages/tables/jquery-datatable.js"></script>
<!-- Demo Js -->
</body>

</html>

