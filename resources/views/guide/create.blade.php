@extends('layouts.dashboard')
<body class="light rtl">
<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">

                        <form method="POST" action="{{ route('create-customer-guide') }}" enctype="multipart/form-data">
                            @csrf
                            <h2 class="card-inside-title">عنوان راهنما </h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control" required
                                                   placeholder="عنوان راهنما">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="card-inside-title">آپلود فایل</h2>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>فایل</span>
                                            <input name="photo" multiple type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-button">
                                <button type="submit" class="btn btn-primary">
                                    ایجاد
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
