@extends('layouts.dashboard')
<body class="light rtl">
<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">


                            <div class="row clearfix">
                                <div class="col-sm-3">
                                    <h2 class="card-inside-title"> نام</h2>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control"
                                                   placeholder="نام درخولست دهنده" value="{{$message->name}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <h2 class="card-inside-title"> موضوع</h2>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control"
                                                   placeholder="موضوع" value="{{$message->subject}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <h2 class="card-inside-title"> آدرس الکترونیکی</h2>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control"
                                                   placeholder="آدرس الکترونیکی" value="{{$message->email}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <h2 class="card-inside-title"> تلفن </h2>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control"
                                                   placeholder="تلفن" value="{{$message->phone}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="card-inside-title">متن پیام </h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="description" class="form-control"
                                                   placeholder="متن پیام" value="{{$message->message}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-button">
                                <a href="/admin-contact" class="btn btn-primary">بازگشت</a>

                            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
