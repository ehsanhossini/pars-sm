@extends('layouts.dashboard')
<body class="light rtl">
<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">

                        <form method="POST" action="{{ route('create-product') }}" enctype="multipart/form-data">
                            @csrf
                            <h2 class="card-inside-title">عنوان دسته بندی</h2>
                            <div class="row clearfix">
                                <div class="col-md-3">

                                    <select class="browser-default" name="category" required>
                                        <option value="" disabled selected>دسته بندی محصول را انتخاب کنید</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" >{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <h2 class="card-inside-title">عنوان محصول</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control" required
                                                   placeholder="عنوان محصول">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="card-inside-title">توضیحات محصول</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="description" class="form-control" required
                                                   placeholder="توضیحات">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="card-inside-title">آپلود فایل</h2>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>فایل</span>
                                                <input name="photo[]" multiple type="file">

                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                </div>
                            </div>

                            <div class="form-button">
                                <button type="submit" class="btn btn-primary">
                                    ایجاد محصول
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
