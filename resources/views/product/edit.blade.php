@extends('layouts.dashboard')
<style>
    body {
        padding: 20px;
    }
    .image-area {
        position: relative;
        width: 15%;
        text-align: center;
        background: #333;
    }
    .image-area img{
        max-width: 100%;
        height: auto;
        width: 110px;
    }
    .remove-image {
        display: none;
        position: absolute;
        top: -10px;
        right: -10px;
        border-radius: 10em;
        padding: 2px 6px 3px;
        text-decoration: none;
        font: 700 21px/20px sans-serif;
        background: #555;
        border: 3px solid #fff;
        color: #FFF;
        box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
        text-shadow: 0 1px 2px rgba(0,0,0,0.5);
        -webkit-transition: background 0.5s;
        transition: background 0.5s;
    }
    .remove-image:hover {
        background: #E54E4E;
        padding: 3px 7px 5px;
        top: -11px;
        right: -11px;
    }
    .remove-image:active {
        background: #E54E4E;
        top: -10px;
        right: -11px;
    }
</style>
<body class="light rtl">
<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">

                        <form method="POST"  action="{{url('edit-product')}}/{{$product->id}}" enctype="multipart/form-data">
                            @csrf
                            <h2 class="card-inside-title">عنوان دسته بندی</h2>
                            <div class="row clearfix">
                                <div class="col-md-3">

                                    <select class="browser-default" name="category">
                                        <option value="" disabled selected>دسته بندی محصول را انتخاب کنید</option>
                                        @foreach($categories as $category)
                                            <option @if($product->category->id == $category->id){{'selected'}}@endif  value="{{ $category->id  }}">
                                                {{ $category->title }}
                                            </option>
                                        @endforeach


                                    </select>
                                </div>
                            </div>

                            <h2 class="card-inside-title">عنوان محصول</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control"
                                                   placeholder="عنوان محصول" value="{{$product->title}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="card-inside-title">توضیحات محصول</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="description" class="form-control"
                                                   placeholder="توضیحات" value="{{$product->description}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="card-inside-title">آپلود فایل</h2>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>فایل</span>
                                            <input name="photo[]" multiple type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" name="image" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="image-area">
                                        @if(count($product_images) > 0)
                                            @foreach($product_images as $product_image)
                                                <img class="product_img" src="{{'/product_images/'.$product_image->image}}" alt="{{$product_image->alt}}">
                                                <a class="remove-image" href="/delete-product-image/{{$product_image->id}}" style="display: inline;">&#215;</a>
                                            @endforeach
                                        @endif

{{--                                        <img class="product_img" src="../../assets/images/products/p-13.jpg" alt="Preview">--}}
{{--                                        <a class="remove-image" href="#" style="display: inline;">&#215;</a>--}}
                                    </div>
                                </div>

                            </div>

                            <div class="form-button">
                                <button type="submit" class="btn btn-primary">
                                    ایجاد محصول
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
