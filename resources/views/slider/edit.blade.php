@extends('layouts.dashboard')
<body class="light rtl">
<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">

                        <form method="POST"  action="{{url('edit-slider')}}/{{$slider->id}}">
                            @csrf

                            <h2 class="card-inside-title">عنوان اسلایدر</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control"
                                                   placeholder="عنوان اسلایدر" value="{{$slider->title}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h2 class="card-inside-title">توضیحات </h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="description" class="form-control"
                                                   placeholder="توضیحات" value="{{$slider->description}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-button">
                                <button type="submit" class="btn btn-primary">
                                    ویرایش اسلایدر
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
