<?php

use App\Models\ProductCategory;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});


Route::get('customer-guide', [App\Http\Controllers\HomeController::class, 'customerGuideList'])->name('home');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/contact-us', function (){
    return view('contact-us');
})->name('contact-us');
Route::post('contact-us',[App\Http\Controllers\HomeController::class, 'storeContact'])->middleware( 'throttle:30,1')->name('store-contact-us');

Route::get('/about-us', function (){
    return view('about-us');
})->name('about-us');


Route::get('/products/{category_id}', [App\Http\Controllers\HomeController::class, 'productList'])->name('products');
Auth::routes();


Route::get('/login', [App\Http\Controllers\AuthController::class, 'index'])->name('login-index');
Route::post('/login', [App\Http\Controllers\AuthController::class, 'login'])->name('login');



Route::group(['middleware' => ['auth:web']], function () {
    Route::get('dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard-index');
    Route::get('create-product', [App\Http\Controllers\DashboardController::class, 'create'])->name('create-product-view');
    Route::post('create-product', [App\Http\Controllers\DashboardController::class, 'store'])->name('create-product');
    Route::get('delete-product/{product_id}', [App\Http\Controllers\DashboardController::class, 'delete'])->name('delete-product');
    Route::get('edit-product/{product_id}', [App\Http\Controllers\DashboardController::class, 'edit'])->name('edit-product');
    Route::post('edit-product/{product_id}', [App\Http\Controllers\DashboardController::class, 'update'])->name('update-product');
    Route::get('delete-product-image/{product_image_id}', [App\Http\Controllers\DashboardController::class, 'deleteProductImage'])->name('delete-product-image');


    Route::get('admin-news', [App\Http\Controllers\NewsController::class, 'index'])->name('admin-news');
    Route::get('create-news', [App\Http\Controllers\NewsController::class, 'create'])->name('create-news-view');
    Route::post('create-news', [App\Http\Controllers\NewsController::class, 'store'])->name('create-news');
    Route::get('delete-news/{news_id}', [App\Http\Controllers\NewsController::class, 'delete'])->name('delete-news');
    Route::get('edit-news/{news_id}', [App\Http\Controllers\NewsController::class, 'edit'])->name('edit-news');
    Route::post('edit-news/{news_id}', [App\Http\Controllers\NewsController::class, 'update'])->name('update-news');


    Route::get('admin-contact', [App\Http\Controllers\ContactController::class, 'index'])->name('admin-contact');
    Route::get('delete-contact/{contact_id}', [App\Http\Controllers\ContactController::class, 'delete'])->name('delete-contact');
    Route::get('show-contact/{contact_id}', [App\Http\Controllers\ContactController::class, 'detail'])->name('view-contact');

    Route::get('admin-customer-guide', [App\Http\Controllers\CustomerGuideController::class, 'index'])->name('customer-guide');
    Route::get('create-customer-guide', [App\Http\Controllers\CustomerGuideController::class, 'create'])->name('create-customer-guide-view');
    Route::post('create-customer-guide', [App\Http\Controllers\CustomerGuideController::class, 'store'])->name('create-customer-guide');
    Route::get('edit-customer-guide/{guide_id}', [App\Http\Controllers\CustomerGuideController::class, 'edit'])->name('edit-customer-guide');
    Route::post('edit-customer-guide/{guide_id}', [App\Http\Controllers\CustomerGuideController::class, 'update'])->name('update-customer-guide');
    Route::get('delete-customer-guide/{contact_id}', [App\Http\Controllers\CustomerGuideController::class, 'delete'])->name('delete-customer-guide');
    Route::get('show-customer-guide/{contact_id}', [App\Http\Controllers\CustomerGuideController::class, 'detail'])->name('view-customer-guide');
    Route::get('delete-customer-guide-file/{guide_id}', [App\Http\Controllers\CustomerGuideController::class, 'deleteGuideFile'])->name('delete-customer-guide-file');

    Route::get('admin-slider', [App\Http\Controllers\SliderController::class, 'index'])->name('admin-slider');
    Route::get('create-slider', [App\Http\Controllers\SliderController::class, 'create'])->name('create-slider-view');
    Route::post('create-slider', [App\Http\Controllers\SliderController::class, 'store'])->name('create-slider');
    Route::get('delete-slider/{slider_id}', [App\Http\Controllers\SliderController::class, 'delete'])->name('delete-slider');
    Route::get('edit-slider/{slider_id}', [App\Http\Controllers\SliderController::class, 'edit'])->name('edit-slider');
    Route::post('edit-slider/{slider_id}', [App\Http\Controllers\SliderController::class, 'update'])->name('update-slider');

});
