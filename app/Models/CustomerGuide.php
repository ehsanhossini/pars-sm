<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerGuide extends Model
{
    protected $fillable = ['title', 'file'];

    const PUBLIC_PATH = 'customer_guides';

    use HasFactory;
}
