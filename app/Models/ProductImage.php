<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;

    const PUBLIC_PATH = 'product_images';

    protected $fillable = [
        'product_id', 'image','alt'
    ];
}
