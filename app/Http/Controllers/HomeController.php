<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\CustomerGuide;
use App\Models\News;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\SlideShow;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = ProductCategory::all();
        $products = Product::all();
        $news = News::all();
        $sliders = SlideShow::all();
        return view('welcome', ['categories' => $categories, 'products' => $products, 'news' => $news, 'sliders' => $sliders]);
    }

    public function storeContact(Request $request)
    {
        Contact::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=> $request->phone,
            'subject' => $request->subject,
            'message' => $request->message,
        ]);
        return redirect()->route('contact-us');
    }

    public function productList($category_id)
    {
        $categories = ProductCategory::all();
        $products = Product::where('category_id',$category_id)->get();
        $news = News::all();
        return view('product-list', ['categories'=>$categories,'products' => $products, 'news' => $news]);
    }

    public function customerGuideList()
    {
        $guides = CustomerGuide::all();
        return view('guide-list', ['guides' => $guides]);
    }

}
