<?php

namespace App\Http\Controllers;


use App\Models\News;
use App\Models\SlideShow;
use Illuminate\Http\Request;

class SliderController extends Controller
{

    public function index()
    {
        $slider = SlideShow::all();
        return view('admin-slider', ['sliders' => $slider]);
    }

    public function create()
    {
        return view('slider.create');
    }

    public function store(Request $request)
    {
        $title = $request->title;
        $description = $request->description;
        SlideShow::create([
            'title' => $title,
            'description' => $description
        ]);
        return redirect()->route('admin-slider');
    }

    public function delete($id)
    {
        $slider_row = SlideShow::find($id);
        $slider_row->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $slider = SlideShow::find($id);
        return view('slider.edit', ['slider' => $slider]);
    }

    public function update($news_id,Request $request)
    {
        $slider_row = SlideShow::find($news_id);
        $slider_row->title = $request->title;
        $slider_row->description = $request->description;
        $slider_row->save();
        return redirect()->route('admin-slider');
    }





}
