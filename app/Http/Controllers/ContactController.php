<?php

namespace App\Http\Controllers;


use App\Models\Contact;
use App\Models\News;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        $messages = Contact::all();
        return view('admin-contact', ['messages' => $messages]);
    }



    public function delete($id)
    {
        $contact_row = Contact::find($id);
        $contact_row->delete();
        return redirect()->route('admin-contact');
    }

    public function detail($id)
    {
        $contact_row = Contact::find($id);
        $contact_row->update([
            'read'=>true
        ]);
        return view('contacts.detail', ['message' => $contact_row]);
    }



}
