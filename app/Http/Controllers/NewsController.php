<?php

namespace App\Http\Controllers;


use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function index()
    {
        $news = News::all();
        return view('admin-news', ['news' => $news]);
    }

    public function create()
    {
        return view('news.create');
    }

    public function store(Request $request)
    {
        $title = $request->title;
        $description = $request->description;
        News::create([
            'title' => $title,
            'description' => $description
        ]);
        return redirect()->route('admin-news');
    }

    public function delete($id)
    {
        $news_row = News::find($id);
        $news_row->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $news = News::find($id);
        return view('news.edit', ['news' => $news]);
    }

    public function update($news_id,Request $request)
    {
        $news_row = News::find($news_id);
        $news_row->title = $request->title;
        $news_row->description = $request->description;
        $news_row->save();
        return redirect()->route('admin-news');
    }





}
