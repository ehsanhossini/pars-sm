<?php

namespace App\Http\Controllers;


use App\Models\Contact;
use App\Models\CustomerGuide;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use File;

class CustomerGuideController extends Controller
{

    public function index()
    {
        $guides = CustomerGuide::all();
        return view('admin-guide', ['guides' => $guides]);
    }

    public function create()
    {
        return view('guide.create');
    }

    public function store(Request $request)
    {
        $category = $request->category;
        $title = $request->title;
        $file = '';
        /*===================== store in accommodation-media ===========================*/
        if ($request->photo) {
           $file= $this->createUploadPhoto($request->photo);
        }
        CustomerGuide::create([
            'title' => $title,
            'file' => $file
        ]);
        return redirect()->route('customer-guide');
    }

    public function edit($id)
    {
        $guide = CustomerGuide::find($id);
        return view('guide.edit', ['guide' => $guide]);
    }

    public function update($guide_id,Request $request)
    {
        $guide = CustomerGuide::find($guide_id);
        $guide->title = $request->title;
        /*===================== store in customer-guide file ===========================*/
        if ($request->photo) {
            $guide->file = $this->createUploadPhoto($request->photo);
        }
        $guide->save();
        return redirect()->route('customer-guide');
    }


    public function delete($id)
    {
        $guide = CustomerGuide::find($id);
        unlink(public_path(CustomerGuide::PUBLIC_PATH.'/' . $guide->file));
        $guide->delete();
        return redirect()->route('customer-guide');
    }

    public function detail($id)
    {
        $contact_row = Contact::find($id);
        $contact_row->update([
            'read'=>true
        ]);
        return view('contacts.detail', ['message' => $contact_row]);
    }


    protected function createUploadPhoto($image)
    {
        $today = base64_encode(date("Y-m-d", time()));
        $today = rtrim($today, '=');
        /*======================== save photo in path ==========================*/
        $uuid = Uuid::uuid1();
        $uuid = str_replace(["-", ""], '', $uuid);
        $input['imageName'] = $uuid . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path(CustomerGuide::PUBLIC_PATH);
        if (!File::exists($destinationPath)) {
            $path = public_path($destinationPath);
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $image->move($destinationPath, $input['imageName']);
        return $input['imageName'];
    }

  public function deleteGuideFile($guide_id)
  {
      if ($guide = CustomerGuide::find($guide_id)) {
          unlink(public_path(CustomerGuide::PUBLIC_PATH.'/' . $guide->file));
      }
      return view('guide.edit', ['guide' => $guide]);
  }



}
