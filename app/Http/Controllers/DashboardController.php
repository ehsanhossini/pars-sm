<?php

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use File;

class DashboardController extends Controller
{

    public function index()
    {
        $products = Product::all();
        return view('products', ['products' => $products]);
    }

    public function create()
    {
        $categories = ProductCategory::all();
        return view('product.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $category = $request->category;
        $title = $request->title;
        $description = $request->description;
        $product = Product::create([
            'category_id' => $category,
            'title' => $title,
            'description' => $description
        ]);
        /*===================== store in accommodation-media ===========================*/
        if ($request->photo) {
            $this->createUploadPhoto($request->photo,$product);
        }
        return redirect()->route('dashboard-index');
    }

    public function delete($id)
    {
        $product = Product::find($id);
        if($product_images = ProductImage::where('product_id',$product->id)->get()){
            foreach ($product_images as $product_image){
                unlink(public_path(ProductImage::PUBLIC_PATH.'/' . $product_image->image));
                $product_image->delete();
            }

        }
        $product->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $product_image = ProductImage::where('product_id',$product->id)->get();
        $categories = ProductCategory::all();
        return view('product.edit', ['product' => $product, 'categories' => $categories, 'product_images' => $product_image]);
    }

    public function update($product_id,Request $request)
    {
        $product = Product::find($product_id);
        $product->title = $request->title;
        $product->description = $request->description;
        $product->category_id = $request->category;
        $product->save();
        /*===================== store in accommodation-media ===========================*/
        if ($request->photo) {
            $this->createUploadPhoto($request->photo,$product);
        }
        return redirect()->route('dashboard-index');
    }

    public function deleteProductImage($product_image_id)
    {
        if ($product = ProductImage::find($product_image_id)) {
            unlink(public_path(ProductImage::PUBLIC_PATH.'/' . $product->image));
        }
        $product->delete();
        return redirect()->back();
    }

    protected function createUploadPhoto($images,$product)
    {
        $today = base64_encode(date("Y-m-d", time()));
        $today = rtrim($today, '=');
        foreach ($images as $image) {
            /*======================== save photo in path ==========================*/
            $uuid = Uuid::uuid1();
            $uuid = str_replace(["-", ""], '', $uuid);

            $input['imageName'] = $uuid . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path(ProductImage::PUBLIC_PATH);
            if (!File::exists($destinationPath)) {
                $path = public_path($destinationPath);
                File::makeDirectory($path, $mode = 0777, true, true);
            }

            $image->move($destinationPath, $input['imageName']);

            /*===================== store in media =====================*/
            ProductImage::create([
                'product_id' => $product->id,
                'image' => $input['imageName'],
                'alt' => $product->title,
            ]);
        }
    }

}
