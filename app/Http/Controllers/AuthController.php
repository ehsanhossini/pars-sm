<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            {
//                $user = auth()->user();
//                dd($user->id,$user->name,$user->email);
                $request->session()->regenerate();
                return redirect()->intended('dashboard');
            }
            else
            {
//                $this->flash_message('danger', 'Log In Failed. Please Check Your Username/Password'); // Call flash message function
                return redirect('/login'); // Redirect to login page
            }

//        else
//        {
//            $this->helper->flash_message('danger', 'Log In Failed. You are Blocked by Admin.'); // Call flash message function
//            return redirect(ADMIN_URL.'/login'); // Redirect to login page
//        }


//        return view('auth.login');
    }

}
